<body>

<div class="container">

    <form action="process.php" method="POST" enctype="multipart/form-data">
        <?php
        if (isset($_GET['success_msg'])) {
            echo "<span class='success_msg'>File Uploaded Successfully</span>";
        }
        ?>
        <input type="file" name="fileUpload[]" multiple="multiple">
        <input type="submit" value="Upload" name="file_upload">
    </form>
</div>
<div class="list">
    <table class="table">
        <tr>
            <th>Serial No</th>
            <th>File Name</th>
            <th>Action</th>
        </tr>
        <?php

        $directory = opendir("uploads");
        if ($directory) {
            $i = 0;
            while(($fileList = readdir($directory)) !== false) {
                if ($fileList != '.' && $fileList != '..') {
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td><a href=\"uploads/$fileList\">$fileList</a></td>";
                    echo "<td><a href='' onClick='delete()'>Delete</a></td>";
                    echo "</tr>";
                }
                $i++;
            }
            closedir($directory);
        }

        ?>
    </table>
</div>

</body>
</html>